FROM python:3.9-slim

WORKDIR /opt/veterina

COPY requirements.txt  .
RUN pip3 install -r requirements.txt

COPY clinic/ .


CMD [ "gunicorn", "clinic.wsgi" ]
